import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'user' })
export class CategoryPipe implements PipeTransform {
    transform(users: any, searchText: any): any {
    if(searchText == null) return users;

    return users.filter(function(user){
        return user.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
    })
    }
}