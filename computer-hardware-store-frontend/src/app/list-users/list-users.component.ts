import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router ) { }

  users = [];
  usersCopy = [];
  images = [];
  searchTerm: string = '';
  ngOnInit() {
    this.fetchData();
  }

  fetchData(){
    this.http.get('http://localhost:8080/api/users')
    .subscribe(
       data => {
        console.log(data);
        for (const key in data) {
          if (data.hasOwnProperty(key)) {
            // tslint:disable-next-line:no-shadowed-variable
            const element = data[key];
            console.log(element.name);
            this.users.push({'id': element.id, 'name' : element.name, 'address' : element.address});
          }
        }
      }
    );
  }

  deleteUser (obj) {
    var userId = obj.target.attributes.data.value;
    if(confirm("Are you sure you want to delete the selected user with id: " + userId + "?")){
      this.http.delete('http://localhost:8080/api/users/' + userId)
      .subscribe(data=>{
        console.log(data);
        this.users = [];
        this.fetchData();
      });
    }else{
      console.log("cancel");
    }
  };

  search(): void {
    this.usersCopy = this.users;
    let term = this.searchTerm;
    if (term == ''){
      this.users = [];
      this.fetchData();
    }
    for(let user in this.users){
      if(this.users[user].name.toLowerCase().indexOf(term.toLowerCase()) == -1){
        this.removeItem(this.users[user]);
      }
    }
  }

  removeItem(doc){
    this.users.forEach( (user, index) => {
      if(user === doc) this.users.splice(index,1);
    });
  }
}
