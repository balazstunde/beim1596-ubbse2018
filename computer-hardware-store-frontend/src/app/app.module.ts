import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule} from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListUsersComponent } from './list-users/list-users.component';
import { RouterModule, Routes} from '@angular/router';
import { ListProductsComponent } from './list-products/list-products.component';
import { NewProductComponent } from './new-product/new-product.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BuyProductComponent } from './buy-product/buy-product.component';

const routes: Routes = [
  { path: 'buy-something', component: BuyProductComponent},
  { path: 'new-product', component: NewProductComponent },
  { path: 'products', component: NewProductComponent },
  { path: 'users', component: ListUsersComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    ListUsersComponent,
    ListProductsComponent,
    NewProductComponent,
    BuyProductComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
