import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators, NgForm } from '@angular/forms';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-new-product',
  templateUrl: './new-product.component.html',
  styleUrls: ['./new-product.component.css']
})
export class NewProductComponent implements OnInit {

  public productForm: FormGroup;
  products = [];
  constructor(private _fb: FormBuilder, private http: HttpClient) { }

  ngOnInit() {
    this.fetchData();
    this.productForm = this._fb.group({
        name: new FormControl('', Validators.required ),
        description: new FormControl('', Validators.required ),
        price: new FormControl('', Validators.compose(
          [Validators.min(0), Validators.required]))
    });

  }

  fetchData(){
    this.http.get('http://localhost:8080/api/products')
    .subscribe(
       data => {
        console.log(data);
        for (const key in data) {
          if (data.hasOwnProperty(key)) {
            // tslint:disable-next-line:no-shadowed-variable
            const element = data[key];
            console.log(element.name);
            this.products.push({'id': element.id, 'name' : element.name, 'description' : element.description, 'price' : element.price});
          }
        }
      }
    );
  }

  onSubmit(f: NgForm) {
      console.log(f.value);
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json'
      })
      };
      this.http.post("http://localhost:8080/api/products/", f.value, httpOptions)
      .subscribe(
        data => {
          this.products=[];
          this.fetchData();
        }
      );

  }

  deleteProduct (obj) {
    var productId = obj.target.attributes.data.value;
    if(confirm("Are you sure you want to delete the selected user with id: " + productId + "?")){
      this.http.delete('http://localhost:8080/api/products/' + productId)
      .subscribe(data=>{
        console.log(data);
        this.products = [];
        this.fetchData();
      });
    }else{
      console.log("cancel");
    }
  };

}
