import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.css']
})
export class ListProductsComponent implements OnInit {

  constructor(private http: HttpClient, private router: Router ) { }

  products = [];
  ngOnInit() {
    this.fetchData();
  }

  fetchData(){
    this.http.get('http://localhost:8080/api/products')
    .subscribe(
       data => {
        console.log(data);
        for (const key in data) {
          if (data.hasOwnProperty(key)) {
            // tslint:disable-next-line:no-shadowed-variable
            const element = data[key];
            console.log(element.name);
            this.products.push({'id': element.id, 'name' : element.name, 'description' : element.description, 'price' : element.price});
          }
        }
      }
    );
  }

  deleteProduct (obj) {
    var productId = obj.target.attributes.data.value;
    if(confirm("Are you sure you want to delete the selected user with id: " + productId + "?")){
      this.http.delete('http://localhost:8080/api/products/' + productId)
      .subscribe(data=>{
        console.log(data);
        this.products = [];
        this.fetchData();
      });
    }else{
      console.log("cancel");
    }
  };

}
