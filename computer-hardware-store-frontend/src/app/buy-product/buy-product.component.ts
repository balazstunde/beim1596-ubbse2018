import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Product} from "./product.model";

@Component({
  selector: 'app-buy-product',
  templateUrl: './buy-product.component.html',
  styleUrls: ['./buy-product.component.css']
})
export class BuyProductComponent implements OnInit {

  constructor(private http: HttpClient,) { }
  products = [];
  cart = JSON.parse(localStorage.getItem('cart')) || [];
  //product: Product;

  ngOnInit() {
    this.fetchData();
    //window.localStorage.clear();
    if(!localStorage.getItem('userId'))
      localStorage.setItem('userId', '1');

  }

  fetchData(){
    this.http.get('http://localhost:8080/api/products')
    .subscribe(
       data => {
        console.log(data);
        for (const key in data) {
          if (data.hasOwnProperty(key)) {
            // tslint:disable-next-line:no-shadowed-variable
            const element = data[key];
            console.log(element.name);
            this.products.push({'id': element.id, 'name' : element.name, 'description' : element.description, 'price' : element.price});
          }
        }
      }
    );
  }

  addToCart(obj){
    var prodId = obj.target.attributes.data.value;
    var newProduct = this.products.find(x => x.id == prodId);
    this.cart.push(newProduct);
    localStorage.setItem('cart', JSON.stringify(this.cart));
    console.log("Cart: " + localStorage.getItem('cart'));
    var product = {
      productId:prodId,
      userId:+localStorage.getItem('userId')
    };
    this.http.post('http://localhost:8080/api/cart', product)
    .subscribe(
      data=>{
        this.products = [];
        this.fetchData();
      }
    );
    //const data = JSON.parse(localStorage.getItem('items'));
  }

}
