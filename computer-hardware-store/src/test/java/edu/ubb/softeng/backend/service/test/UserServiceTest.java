package edu.ubb.softeng.backend.service.test;


import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import edu.ubb.softeng.backend.model.User;
import edu.ubb.softeng.backend.service.UserService;
import edu.ubb.softeng.backend.repository.UserRepository;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {
	@Mock
    private UserRepository userRepository;
	
    @InjectMocks
    private UserService userService;
    
    List<User> users;
    
    @Before
    public void setUsers() {
    	users = new ArrayList<>();
    	users.add(new User( 1, "Simo Anna", "simoanna", "Kolozsvar"));
    	users.add(new User( 2, "Gergely Pal", "gergelyPal", "Csikszereda"));
    }
	
	@Test
	public void testGetUser() {
		User user = new User();
		user.setId(1);
		user.setName("Simo Anna");
		user.setUsername("simoanna");
		user.setAddress("Kolozsvar");
		when(userRepository.findById(1)).thenReturn(Optional.of(users.get(0)));
        
        User user1 = userService.getUser(1);
		assertThat(user1, samePropertyValuesAs(user));
	}
	
	@Test
	public void testAddUser() {
		User user = new User();
		user.setId(3);
		user.setName("Simo Anna1");
		user.setUsername("simoanna1");
		user.setAddress("Kolozsvar1");
		when(userRepository.save(user)).thenAnswer(invocation -> {
		    users.add(user);
		     return user;
		});
		
		int db = users.size();
		userService.addUser(user);
		
        assertTrue(users.contains(user));
        assertEquals(db + 1, users.size());
	}
	
	@Test
	public void testGetUsers() {
		when(userRepository.findAll()).thenReturn(users);
		
		List<User> users1 = userService.getUsers();
		assertArrayEquals(users.toArray(), users1.toArray());
		
	}
}
