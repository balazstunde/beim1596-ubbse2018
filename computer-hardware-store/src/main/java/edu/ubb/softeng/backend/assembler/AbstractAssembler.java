package edu.ubb.softeng.backend.assembler;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;


@Component
public abstract class AbstractAssembler<M, D> {
	
	public D modelToDto(M model) {
		throw new UnsupportedOperationException("Conversion not possible");
	}
	
	public M dtoToModel(D dto) {
		throw new UnsupportedOperationException("Conversion not possible");
	}
	
	public Collection<D> modelsToDtos(Collection<M> models) {
		List<D> results = models.stream().map(model -> modelToDto(model)).collect(Collectors.toList());
		return results;
	}
	
	public Collection<M> dtosToModels(Collection<D> dtos) {
		List<M> results = dtos.stream().map(dto -> dtoToModel(dto)).collect(Collectors.toList());
		return results;
	}
}