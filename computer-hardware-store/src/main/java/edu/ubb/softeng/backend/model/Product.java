package edu.ubb.softeng.backend.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity(name = "products")
public class Product extends BaseEntity {
	
	@Column(name = "p_name")
	private String name;
	
	@Column(name = "p_description")
	private String description;
	
	@Column(name = "price")
	private Float price;
	
	public Product() {
		
	}

	public Product(Integer id, String name, String description, Float price) {
		super();
		this.id = id;
		this.name = name;
		this.description = description;
		this.price = price;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Float getPrice() {
		return price;
	}

	public void setPrice(Float price) {
		this.price = price;
	}
}
