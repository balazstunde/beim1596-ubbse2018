package edu.ubb.softeng.backend.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.ubb.softeng.backend.model.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer>{
	
}
