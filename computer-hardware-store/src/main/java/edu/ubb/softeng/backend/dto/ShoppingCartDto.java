package edu.ubb.softeng.backend.dto;

import java.io.Serializable;

public class ShoppingCartDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Integer userId;
	private Integer productId;
	
	public ShoppingCartDto() {
		
	}

	public ShoppingCartDto(Integer userId, Integer productId) {
		super();
		this.userId = userId;
		this.productId = productId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

}
