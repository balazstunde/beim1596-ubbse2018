package edu.ubb.softeng.backend.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.ubb.softeng.backend.assembler.UserAssembler;
import edu.ubb.softeng.backend.dto.UserDto;
import edu.ubb.softeng.backend.model.User;
import edu.ubb.softeng.backend.service.UserService;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class UserController {

	@Autowired
	private UserService userService;
	
	@Autowired
	private UserAssembler userAssembler;
	
	@RequestMapping (value = "/home", method = RequestMethod.GET)
	@ResponseBody
	public String hello() {
		return "Hello world!";
	}
	
	@RequestMapping (value = "/users/{userId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteUser(@PathVariable Integer userId) {
		User user = userService.getUser(userId);
		userService.deleteUser(user);
	}
	
	@RequestMapping (value = "/users", method = RequestMethod.POST)
	@ResponseBody
	public void addUser(@RequestBody UserDto userDto) {
		User user = userAssembler.dtoToModel(userDto);
		userService.addUser(user);
	}
	
	@RequestMapping (value = "/users", method = RequestMethod.GET)
	@ResponseBody
	public Collection<UserDto> getAllUsers() {
		return userAssembler.modelsToDtos(userService.getUsers());
	}
}
