package edu.ubb.softeng.backend.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import edu.ubb.softeng.backend.model.ShoppingCart;

@Repository
public interface ShoppingCartRepository extends CrudRepository<ShoppingCart, Integer> {
	public List<ShoppingCart> findByUserId(Integer id);
}
