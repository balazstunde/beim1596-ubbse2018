package edu.ubb.softeng.backend.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.ubb.softeng.backend.dto.UserDto;
import edu.ubb.softeng.backend.model.User;

@Component
public class UserAssembler extends AbstractAssembler<User, UserDto>{

	@Autowired
	private ModelMapper modelMapper;
	
	@Override
	public UserDto modelToDto(User model) {
		UserDto userDto = modelMapper.map(model, UserDto.class);
		return userDto;
	}
	
	@Override
	public User dtoToModel(UserDto dto) {
		User user = modelMapper.map(dto, User.class);
		return user;
	}
}
