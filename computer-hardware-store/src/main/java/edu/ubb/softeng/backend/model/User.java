package edu.ubb.softeng.backend.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

@Entity(name = "users")
public class User extends BaseEntity{
	
	@Column(name = "u_name")
	private String name;
	
	@Column(name = "username")
	private String username;
	
	@Column(name = "address")
	private String address;
	
	@OneToMany(fetch = FetchType.EAGER)
	@JoinColumn(name="userId")
	private List<ShoppingCart> shoppingCart;
	
	public User() {
		
	}

	public User(Integer id, String name, String username, String address) {
		super();
		this.id = id;
		this.name = name;
		this.username = username;
		this.address = address;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
}
