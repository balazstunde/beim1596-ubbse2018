package edu.ubb.softeng.backend.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ubb.softeng.backend.model.ShoppingCart;
import edu.ubb.softeng.backend.repository.ShoppingCartRepository;

@Service
public class ShoppingCartService {

	@Autowired
	private ShoppingCartRepository shoppingCartRepository;
	
	public ShoppingCart getShoppingCart(Integer shoppingCartId) {
		return shoppingCartRepository.findById(shoppingCartId).get();
	}
	
	public ShoppingCart addShoppingCart(ShoppingCart shoppingCart) {
		return shoppingCartRepository.save(shoppingCart);
	}
	
	public void deleteShoppingCart(ShoppingCart shoppingCart) {
		shoppingCartRepository.delete(shoppingCart);
	}
	
	public List<ShoppingCart> getAllShoppingCartByUserId(Integer id){
		List<ShoppingCart> shoppingCarts = new ArrayList<>();
		shoppingCartRepository.findByUserId(id).forEach(shoppingCarts::add);
		
		return shoppingCarts;
	}
}
