package edu.ubb.softeng.backend.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.ubb.softeng.backend.dto.ProductDto;
import edu.ubb.softeng.backend.model.Product;

@Component
public class ProductAssembler extends AbstractAssembler<Product, ProductDto>{

	@Autowired
    private ModelMapper modelMapper;
	
	@Override
	public ProductDto modelToDto(Product model) {
		ProductDto productDto = modelMapper.map(model, ProductDto.class);
	    return productDto;
	}
	
	@Override
	public Product dtoToModel(ProductDto dto) {
		Product product = modelMapper.map(dto, Product.class);
	    return product;
	}
}
