package edu.ubb.softeng.backend.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.ubb.softeng.backend.assembler.ShoppingCartAssembler;
import edu.ubb.softeng.backend.dto.ShoppingCartDto;
import edu.ubb.softeng.backend.model.ShoppingCart;
import edu.ubb.softeng.backend.service.ShoppingCartService;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class CartController {

	@Autowired
	private ShoppingCartService shoppingCartService;
	
	@Autowired
	private ShoppingCartAssembler shoppingCartAssembler;
	
	@RequestMapping (value = "/cart", method = RequestMethod.POST)
	@ResponseBody
	public ShoppingCartDto addProducts(@RequestBody ShoppingCart shoppingCart) {
		return shoppingCartAssembler.modelToDto(shoppingCartService.addShoppingCart(shoppingCart));
	}
	
	@RequestMapping (value = "/cart/{id}", method = RequestMethod.GET)
	@ResponseBody
	public Collection<ShoppingCartDto> getProducts(@PathVariable Integer id) {
		return shoppingCartAssembler.modelsToDtos(shoppingCartService.getAllShoppingCartByUserId(id));
	}
}
