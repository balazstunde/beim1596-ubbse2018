package edu.ubb.softeng.backend.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import edu.ubb.softeng.backend.assembler.ProductAssembler;
import edu.ubb.softeng.backend.dto.ProductDto;
import edu.ubb.softeng.backend.model.Product;
import edu.ubb.softeng.backend.service.ProductService;

@CrossOrigin
@RestController
@RequestMapping("/api")
public class ProductController {

	@Autowired
	private ProductService productService;
	
	@Autowired
	private ProductAssembler productAssembler;
	
	@RequestMapping (value = "/products/{productId}", method = RequestMethod.DELETE)
	@ResponseBody
	public void deleteProduct(@PathVariable Integer productId) {
		Product product = productService.getProduct(productId);
		productService.deleteProduct(product);
	}
	
	@RequestMapping (value = "/products", method = RequestMethod.GET)
	@ResponseBody
	public Collection<ProductDto> showProducts() {
		return productAssembler.modelsToDtos(productService.getProducts());
	}
	
	@RequestMapping (value = "/products", method = RequestMethod.POST)
	@ResponseBody
	public ProductDto addProducts(@RequestBody Product product) {
		return productAssembler.modelToDto(productService.addProduct(product));
	}
}
