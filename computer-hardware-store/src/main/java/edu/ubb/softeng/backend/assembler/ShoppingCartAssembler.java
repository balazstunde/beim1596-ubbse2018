package edu.ubb.softeng.backend.assembler;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import edu.ubb.softeng.backend.dto.ShoppingCartDto;
import edu.ubb.softeng.backend.model.ShoppingCart;

@Component
public class ShoppingCartAssembler extends AbstractAssembler<ShoppingCart, ShoppingCartDto>{

	@Autowired
	private ModelMapper modelMapper;
	
	@Override
	public ShoppingCartDto modelToDto(ShoppingCart model) {
		ShoppingCartDto shoppingCartDto = modelMapper.map(model, ShoppingCartDto.class);
		return shoppingCartDto;
	}
	
	@Override
	public ShoppingCart dtoToModel(ShoppingCartDto dto) {
		ShoppingCart shoppingCart = modelMapper.map(dto, ShoppingCart.class);
		return shoppingCart;
	}
}
